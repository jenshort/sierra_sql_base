<?php

namespace SierraSql\Controller;

use SierraSql\Controller\Controller;
use SierraSql\Model\SampleModel;
use Symfony\Component\HttpFoundation\Request;	
use Symfony\Component\HttpFoundation\Response;

class SampleController extends Controller
{
	public function __construct()
	{
		parent::__construct();
		// These templates can be accessed via $this->templates->make(sample::<viewname)
		//$this->templates->addFolder('sample', '../app/View/samples');
	}

	public function show(Request $request, Response $response, array $args)
	{
		$report = new SampleModel;
		$data = $report->getData();
		$col_headers = $report->getHeaders();

		// $template = $this->templates->make('shared::report');
		// $template->data(['data' => $data, 'col_headers' => $col_headers]);

		// $response->setContent($template->render());
		$response->setContent('This is the sample controller!');
		$response->setStatusCode(Response::HTTP_OK);

		return $response;	
	}

}