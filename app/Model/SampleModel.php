<?php

namespace SierraSql\Model;
use SierraSql\DbConnection;
use SierraSql\Model\Model;

class SampleModel extends Model
{
	private	$data,
			$rowHeaders;

	public function __construct()
	{
		parent::__construct();
	}

	public function getData($withHeaders = false)
	{	
		if($withHeaders) {
			$data = $this->data;
			array_unshift($data, $this->rowHeaders);
			return $data;
		} else {
			return $this->data;
		}
	}

	public function getHeaders()
	{
		return $this->rowHeaders;
	}

}